{
  description = "Guess the word and save the snake!";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "nixpkgs/nixos-unstable";
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-utils, rust-overlay }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [ (import rust-overlay) ];
        pkgs = import nixpkgs { inherit system overlays; };
        rust = pkgs.rust-bin.fromRustupToolchainFile ./rust-toolchain.toml;
        buildInputs = with pkgs; [
          openssl
          alsa-lib
          wayland
          libxkbcommon
          udev
          vulkan-loader
        ];
        nativeBuildInputs = with pkgs; [
          pkg-config
          trunk
          gnumake
          jq
          cachix
        ];
        rustPlatform = pkgs.makeRustPlatform {
          cargo = rust;
          rustc = rust;
        };
      in
      {
        defaultPackage = rustPlatform.buildRustPackage {
          pname = "word-snake";
          version = "1.0.0";
          src = ./.;

          cargoLock = {
            lockFile = ./Cargo.lock;
          };

          inherit buildInputs nativeBuildInputs;
        };

        devShell = pkgs.mkShell {
          packages = [
            rust
            pkgs.rust-analyzer
          ];

          inherit buildInputs nativeBuildInputs;

          # These are probably specific to my system and shouldn't be here.
          LD_LIBRARY_PATH = "${with pkgs; lib.makeLibraryPath [
            vulkan-loader
          ]}";
          XCURSOR_THEME = "Adwaita"; # Workaround for https://github.com/bevyengine/bevy/issues/4768
          AMD_VULKAN_ICD = "RADV";
        };
      }
    );
}
