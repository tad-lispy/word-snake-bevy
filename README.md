# Word Snake Bevy

This is an attempt to re-implement and improve the Word Snake game I made using Rust and Bevy game engine.

## Motivation

There are two main goals for this project:

  1. Learn Rust and Bevy
  2. Make a interesting, fun game that people will play

## Goals and Questions

  - [ ] Re-implement basic functionality of the original Word Snake game
  - [x] Publish on the web (https://tad-lispy.gitlab.io/word-snake-bevy/)
  - [ ] Publish to F-Droid
  - [ ] Publish to Google Play Store
  - [ ] Publish to Apple App Store
  - [ ] Implement tutorial levels introducing elements of the gameplay one by one
  - [ ] Animate the movement (make it continuous)
  - [ ] Feedback on correct and incorrect letter collected (visual, audio, haptic)

## Various Notes

This section is kind of a brain-dump.

### Combo Control

Turning twice in the same relative direction (say left and left) should result in a U-turn over 2 steps. Kind of as if the control request are queued, but I think max 2 and only if it makes sens (e.g. no opposite). I don't have a clear concept yet, but I feel something like this would improve the UX.

### Tutorial

Make a tutorial that will introduce each element of the game, one by one:

1. Movemnt
2. Collecting letters
3. Filling the sentence
