use bevy::prelude::*;
use bevy_inspector_egui::WorldInspectorPlugin;

pub struct DebugPlugin;

impl Plugin for DebugPlugin {
    fn build(&self, app: &mut App) {
        #[cfg(debug_assertions)]
        {
            println!("Adding the debug plugin");
            app.add_plugin(WorldInspectorPlugin::new());
            app.insert_resource(bevy::log::LogSettings {
                level: bevy::log::Level::DEBUG,
                ..default()
            });
        };
    }
}
