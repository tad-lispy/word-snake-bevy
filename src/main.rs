mod debug;
mod snake_game;

use bevy::prelude::*;
use snake_game::SnakeGame;

fn main() {
    App::new()
        .insert_resource(WindowDescriptor {
            title: "Word Snake".to_string(),
            ..default()
        })
        .add_plugin(debug::DebugPlugin)
        .add_plugins(DefaultPlugins)
        .add_plugin(SnakeGame {
            size: 30,
            speed: 4.0,
        })
        .add_system(bevy::window::close_on_esc)
        .add_system(fullscreen_toggle)
        .run();
}

fn fullscreen_toggle(
    mut windows: ResMut<Windows>,
    keyboard: Res<Input<KeyCode>>
) {
    let window = windows.primary_mut();
    if keyboard.just_pressed(KeyCode::F11) {
        if window.mode() == bevy::window::WindowMode::BorderlessFullscreen {
            window.set_mode(bevy::window::WindowMode::Windowed);
            window.set_cursor_visibility(true);
        } else {
            window.set_mode(bevy::window::WindowMode::BorderlessFullscreen);
            window.set_cursor_visibility(false);
        }
    }
}
