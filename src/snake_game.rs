use std::time::Duration;

use bevy::{prelude::*, time::FixedTimestep};
use iyes_loopless::prelude::*;
use rand::prelude::random;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
enum RunState {
    Playing,
    Lost,
}

pub struct SnakeGame {
    pub speed: f32,
    pub size: u16,
}

impl Default for SnakeGame {
    fn default() -> Self {
        SnakeGame {
            speed: 4.0,
            size: 60,
        }
    }
}

struct ArenaSize(u16);

struct Control {
    direction: Direction,
}

impl Plugin for SnakeGame {
    fn build(&self, app: &mut App) {
        let mut move_step = SystemStage::parallel();
        move_step
            .add_system(
                move_snake
                    .run_in_state(RunState::Playing)
                    .label("move_snake"),
            )
            .add_system(
                feed_snake
                    .run_in_state(RunState::Playing)
                    .label("feed_snake")
                    .after("move_snake"),
            )
            .add_system(
                grow_snake
                    .run_in_state(RunState::Playing)
                    .after("feed_snake"),
            )
            .add_system(end_game.run_in_state(RunState::Playing).after("move_snake"));

        app.add_loopless_state(RunState::Playing)
            .insert_resource(ArenaSize(self.size))
            .insert_resource(SnakeBody::default())
            .insert_resource(Control {
                direction: Direction::East,
            })
            .add_event::<SnakeAteFood>()
            .add_stage_before(
                CoreStage::Update,
                "MoveStep",
                FixedTimestepStage::new(Duration::from_millis(250)).with_stage(move_step),
            )
            .add_startup_system(setup_camera)
            .add_enter_system(RunState::Playing, respawn_snake)
            .add_system(control_snake)
            .add_system(spawn_food.with_run_criteria(FixedTimestep::step(4.0 / self.speed as f64)))
            .add_system(transform_entities)
            .add_enter_system(RunState::Lost, show_menu)
            .add_system(menu_system.run_in_state(RunState::Lost))
            .add_exit_system(RunState::Lost, hide_menu);
    }
}

// CAMERA

fn setup_camera(mut commands: Commands) {
    commands.spawn_bundle(Camera2dBundle::default());
}

// MENU

#[derive(Component)]
struct Menu;

fn show_menu(mut commands: Commands, asset_server: Res<AssetServer>) {
    commands
        .spawn_bundle(ButtonBundle {
            style: Style {
                size: Size::new(Val::Px(200.), Val::Px(80.)),
                justify_content: JustifyContent::Center,
                align_items: AlignItems::Center,
                margin: UiRect::all(Val::Auto),
                ..Default::default()
            },
            color: Color::ANTIQUE_WHITE.into(),
            ..Default::default()
        })
        .with_children(|parent| {
            parent.spawn_bundle(TextBundle::from_section(
                "Again!",
                TextStyle {
                    font: asset_server.load("Cantarell-ExtraBold.otf"),
                    font_size: 40.,
                    color: Color::DARK_GRAY,
                },
            ));
        })
        .insert(Menu);
}

fn menu_system(
    mut interactions: Query<
        (&Interaction, &mut UiColor),
        (Changed<Interaction>, With<Button>),
    >,
    mut commands: Commands,
) {
    for (interaction, mut color) in &mut interactions {
        match *interaction {
            Interaction::Clicked => {
                *color = Color::GREEN.into();
                commands.insert_resource(NextState(RunState::Playing));
            }
            Interaction::Hovered => *color = Color::DARK_GREEN.into(),
            Interaction::None => *color = Color::ANTIQUE_WHITE.into(),
        }
    }
}

fn hide_menu(menu: Query<Entity, With<Menu>>, mut commands: Commands) {
    menu.for_each(|entity| commands.entity(entity).despawn_recursive());
}

// SNAKE

#[derive(Default)]
struct SnakeBody(Vec<Entity>);

#[derive(Component)]
struct SnakeHead;

#[derive(Component)]
struct SnakeSegment;

#[derive(Component, Clone, PartialEq, Eq, Debug)]
struct Position {
    x: i16,
    y: i16,
}

#[derive(Component, Debug)]
struct Movement {
    direction: Direction,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum Direction {
    North,
    East,
    South,
    West,
}

#[derive(Component, Debug)]
struct EntitySize(f32);

struct SnakeAteFood(Entity);

fn respawn_snake(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut body: ResMut<SnakeBody>,
    head: Query<Entity, With<SnakeHead>>,
    segments: Query<Entity, With<SnakeSegment>>,
) {
    // Remove any previous snake
    head.for_each(|entity| commands.entity(entity).despawn());
    segments.for_each(|entity| commands.entity(entity).despawn());
    body.0.clear();

    commands
        .spawn_bundle(ColorMesh2dBundle {
            mesh: meshes.add(shape::Circle::new(1.).into()).into(),
            material: materials.add(Color::CRIMSON.into()),
            transform: Transform::from_xyz(0., 0., 1.),
            ..Default::default()
        })
        .insert(SnakeHead)
        .insert(EntitySize(0.4))
        .insert(Position { x: 0, y: 0 })
        .insert(Movement {
            direction: Direction::East,
        });

    for _ in 0..5 {
        let segment = commands
            .spawn_bundle(ColorMesh2dBundle {
                mesh: meshes.add(shape::Circle::new(1.).into()).into(),
                material: materials.add(Color::CRIMSON.into()),
                transform: Transform::from_xyz(0., 0., 1.),
                ..Default::default()
            })
            .insert(SnakeSegment)
            .insert(EntitySize(0.3))
            .insert(Position { x: 0, y: 0 })
            .id();

        body.0.push(segment);
    }
}

fn control_snake(mut control: ResMut<Control>, keyboard: Res<Input<KeyCode>>) {
    if keyboard.pressed(KeyCode::Up) {
        control.direction = Direction::North;
    };
    if keyboard.pressed(KeyCode::Right) {
        control.direction = Direction::East;
    };
    if keyboard.pressed(KeyCode::Down) {
        control.direction = Direction::South;
    };
    if keyboard.pressed(KeyCode::Left) {
        control.direction = Direction::West;
    };
}

fn move_snake(
    mut head: Query<(&mut Position, &mut Movement), With<SnakeHead>>,
    mut positions: Query<&mut Position, Without<SnakeHead>>,
    body: ResMut<SnakeBody>,
    control: Res<Control>,
) {
    let (mut position, mut movement) = head.single_mut();
    let mut previous_position: Position = position.clone();

    // Update movement direction according to the control direction
    // but if it's the opposite direction, don't!
    if control.direction.opposite() != movement.direction {
        movement.direction = control.direction.clone();
    }

    // Move the head
    match movement.direction {
        Direction::North => position.y += 1,
        Direction::East => position.x += 1,
        Direction::South => position.y -= 1,
        Direction::West => position.x -= 1,
    };

    // Move the body
    for segment in body.0.iter() {
        if let Ok(mut position) = positions.get_mut(*segment) {
            let Position { x, y } = previous_position;
            if *position == previous_position {
                continue;
            };

            previous_position = position.clone();
            position.x = x;
            position.y = y;
        }
    }
}

fn transform_entities(
    windows: Res<Windows>,
    arena_size: Res<ArenaSize>,
    mut entities: Query<(&Position, &EntitySize, &mut Transform)>,
) {
    let window = windows.get_primary().unwrap();
    let shorter_edge = window.width().min(window.height());
    let scale = shorter_edge / arena_size.0 as f32;

    for (position, size, mut transform) in entities.iter_mut() {
        transform.translation.x = position.x as f32 * scale;
        transform.translation.y = position.y as f32 * scale;
        transform.scale = Vec3::splat(scale * size.0);
    }
}

fn feed_snake(
    heads: Query<&Position, With<SnakeHead>>,
    food: Query<(Entity, &Position), With<Food>>,
    mut eating_notification: EventWriter<SnakeAteFood>,
) {
    let head_position = heads.single();
    for (entity, food_position) in food.iter() {
        if head_position == food_position {
            eating_notification.send(SnakeAteFood(entity));
        }
    }
}

fn grow_snake(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut body: ResMut<SnakeBody>,
    heads: Query<&Position, With<SnakeHead>>,
    mut eating_notification: EventReader<SnakeAteFood>,
) {
    for SnakeAteFood(food) in eating_notification.iter() {
        commands.entity(*food).despawn();
        let head_position = heads.single();
        let segment = commands
            .spawn_bundle(ColorMesh2dBundle {
                mesh: meshes.add(shape::Circle::new(1.).into()).into(),
                material: materials.add(Color::CRIMSON.into()),
                transform: Transform::from_xyz(0., 0., 1.),
                ..Default::default()
            })
            .insert(SnakeSegment)
            .insert(EntitySize(0.3))
            .insert(head_position.clone())
            .id();

        body.0.push(segment);
    }
}

fn end_game(
    head: Query<&Position, With<SnakeHead>>,
    body: Query<&Position, With<SnakeSegment>>,
    mut commands: Commands,
) {
    let head_position = head.single();
    for segment_position in &body {
        if segment_position == head_position {
            debug!("Game over");
            commands.insert_resource(NextState(RunState::Lost));
        }
    }
}
// FOOD

#[derive(Component)]
struct Food;

fn spawn_food(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    arena_size: Res<ArenaSize>,
) {
    let arena_half = (arena_size.0 / 2) as i16;
    let x_translation = (random::<f32>() * arena_size.0 as f32) as i16;
    let y_translation = (random::<f32>() * arena_size.0 as f32) as i16;

    commands
        .spawn_bundle(ColorMesh2dBundle {
            mesh: meshes.add(shape::Circle::new(1.).into()).into(),
            material: materials.add(Color::DARK_GREEN.into()),
            ..Default::default()
        })
        .insert(Food)
        .insert(EntitySize(0.2))
        .insert(Position {
            x: x_translation - arena_half,
            y: y_translation - arena_half,
        });
}
impl Direction {
    fn opposite(&self) -> Self {
        match self {
            Direction::North => Direction::South,
            Direction::East => Direction::West,
            Direction::South => Direction::North,
            Direction::West => Direction::East,
        }
    }
}
